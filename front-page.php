<html>
<head>
  <meta charset="utf-8">
  <title>wesignals</title>
  <meta content="wesignals You never walk alone now" name="description">
  <meta content="wesignals" property="og:title">
	<?php
		echo "<link href=\"" . get_template_directory_uri() . "/assets/css/test.min.css\" rel=\"stylesheet\" type=\"text/css\"> ";
		echo "<link rel=\"icon\" type=\"image/png\" href=\"" . get_template_directory_uri() . "/assets/images/logowhitetransparant.png\" />" ;
	?>
  <script async="" src="https://www.google-analytics.com/analytics.js"></script>
  <script src="https://ajax.googleapis.com/ajax/libs/webfont/1.4.7/webfont.js"></script>
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
	<link rel="stylesheet" type="text/css" href="http://fonts.googleapis.com/css?family=Open+Sans:light,semi-bold,bold">
	<link href="//db.onlinewebfonts.com/c/1b4cd06b44538d529084b3599f708643?family=MuseoSansW01-Rounded300" rel="stylesheet" type="text/css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.2.0/jquery.min.js" type="text/javascript"></script>
  <script type="text/javascript"></script>
</head>
<body>
  <div class="hol-fixed-navbar w-nav" data-animation="default" data-collapse="tiny" data-duration="400" id="top">
    <div class="w-container">
      <a class="w-nav-brand w--current" href="/">
        <?php
      		echo "<img alt=\"wesign Logo\" class=\"wesign-logo\" src=\"" . get_template_directory_uri() . "/assets/images/logoblack.jpg\">";
        ?>
      </a>
      <nav class="nav-menu w-nav-menu" role="navigation">
      <a class="hol-nav-link w-nav-link" href="#top" style="max-width: 940px; font-family: Open Sans; color: #47b7e1;">Home</a>
      <a class="hol-nav-link w-nav-link" href="#about" style="max-width: 940px; font-family: Open Sans;">About Us</a>
      <a class="hol-nav-link w-nav-link" href="#whatwedo" style="max-width: 940px; font-family: Open Sans;">What We Do</a>
      <a class="hol-nav-link w-nav-link" href="#howitworks" style="max-width: 940px; font-family: Open Sans;">How it works</a>
      <a class="hol-nav-link w-nav-link" href="#whyus" style="max-width: 940px; font-family: Open Sans;">Why Us</a>
      <a class="hol-nav-link w-nav-link" href="#ourproduct" style="max-width: 940px; font-family: Open Sans;">Our Products</a>
      <a class="hol-nav-link w-nav-link" href="#contactus" style="max-width: 940px; font-family: Open Sans;">Contact Us</a>
      </nav>
      <div class="green-menu-button w-nav-button">
        <div class="hamburger-icon-white w-icon-nav-menu"></div>
      </div>
    </div>
      <div class="w-nav-overlay" data-wf-ignore=""></div>
  </div>

  <div class="hol-hero-section" data-ix="sticky-nav-appear">
    <div class="w-container">
      <div class="hol-hero-content-wrapper">
        <h1 class="hol-hero-h1-white" style="font-family: Open Sans">
          You never walk alone now<br>
        </h1>
        <p class="hollie-hero-para">We send you trading signals for binary option </p>
      </div>
    </div>
  </div>

  <!-- ABOUT -->
  <div class="hol-about-section" id="about">
    <div class="w-container">
      <div class="hol-about-wrapper">
        <h2 class="hol-wwd-section-title" style="margin-bottom: 50px">About Us </h2>
        <p class="hol-about-paragraph">We are a group of financial analysts and traders with a mission to provide a transparent Binary Options education and signals. <br>
          We get together in the end of summer 2015 and since then we have been able to grow both in terms of numbers and quality of services provided.</p>
          <br>
        <p class="hol-about-paragraph">Our mission is to help traders around the world navigate the landscape of Binary Options better by providing them quality education, high-quality signals and by helping them understand the importance of emotion control and money management when trading.</p>
      </div>
    </div>
  </div>

  <!-- INDEX -->
  <div class="hol-market-index-section">
    <div class="w-container">
        <div class="hol-val-wrapper">
          <div class="col-xs-12 col-md-3" style="display:inline-block">
            <div class="hol-market-index-ticker col-xs-6"> Dow: </div>
            <div class="hol-market-index-val col-xs-6" style="color:blue"> 7.91 ^ </div>
          </div>
          <div class="col-xs-12 col-md-3" style="display:inline-block">
            <div class="hol-market-index-ticker col-xs-6"> Nasdaq: </div>
            <div class="hol-market-index-val col-xs-6" style="color:red"> -4.54  v </div>
          </div>
          <div class="col-xs-12 col-md-4" style="display:inline-block">
            <div class="hol-market-index-ticker col-xs-6"> S&P 500: </div>
            <div class="hol-market-index-val col-xs-6" style="color:red"> -2.03 v </div>
          </div>
          <div class="col-xs-12 col-md-4" style="display:inline-block">
            <div class="hol-market-index-ticker col-xs-6"> NYSE Comp: </div>
            <div class="hol-market-index-val col-xs-6" style="color:red"> -10.2123  v </div>
          </div>
          <div class="col-xs-12 col-md-4" style="display:inline-block">
            <div class="hol-market-index-ticker col-xs-6"> Amex Comp: </div>
            <div class="hol-market-index-val col-xs-6" style="color:blue"> 7.2261 ^ </div>
          </div>
        </div>
    </div>
  </div>


    <!-- PHILOSOPHY -->
    <div class="hol-wwd-section" id="howitworks">
      <div class="w-container">
        <div class="hol-wwd-content-wrapper">
          <h2 class="hol-wwd-section-title">How It Works </h2>
          <div class="info-wrapper">
            <div class="icon-wrapper">
  						<?php
  							echo "<img alt=\"wesignals: places\" class=\"icon\" src=\"" . get_template_directory_uri() . "/assets/images/1.png\"> ";
  						?>
            </div>
            <div class="icon-wrapper">
  						<?php
  							echo "<img alt=\"wesignals: places\" class=\"icon\" src=\"" . get_template_directory_uri() . "/assets/images/arrow.png\" style=\"height:80px\"> ";
  						?>
            </div>
            <div class="icon-wrapper">
  						<?php
  							echo "<img alt=\"wesignals: places\" class=\"icon\" src=\"" . get_template_directory_uri() . "/assets/images/2.png\"> ";
  						?>
            </div>
            <div class="icon-wrapper">
  						<?php
  							echo "<img alt=\"wesignals: places\" class=\"icon\" src=\"" . get_template_directory_uri() . "/assets/images/arrow.png\" style=\"height:80px\"> ";
  						?>
            </div>
            <div class="icon-wrapper">
  						<?php
  							echo "<img alt=\"wesignals: places\" class=\"icon\" src=\"" . get_template_directory_uri() . "/assets/images/3.png\"> ";
  						?>
            </div>
          </div>
        </div>
      </div>
    </div>

    <!-- WHY CHOOSE US -->
      <div class="hol-val-wrapper rev" id="whychooseus">
        <div class="hol-val-div">
          <div class="hol-val-content-wrapper">
            <h2 class="why-choose-us hol-wwd-section-title">Why choose us<br>
              <hr style="margin-left:0; color:#dfdfe0; height:5px; background-color:#dfdfe0; width:50px;">
            </h2>
            <?php
              echo "<img alt=\"wesignals: reason to choose us\" src=\"" . get_template_directory_uri() . "/assets/images/reasonchooseus.jpg\">";
             ?>
          </div>
        </div>
        <div class="hol-val-div">
          <?php
            echo "<img alt=\"wesignals: why choose us\" src=\"" . get_template_directory_uri() . "/assets/images/whychooseus.jpg\">";
           ?>
        </div>
      </div>

<!-- PRODUCT -->
  <div class="hol-product-section">
    <div class="w-container">
      <div class="div-block-centered">
        <div class="hol-location-title-wrapper">
          <h2 class="hol-wwd-section-title location">Real-time trade signals from qualified experts</h2>
          <p class="centered hol-val-paragraph" id="pstyle">Wesignal provides daily actionable trade ideas for the European Index Markets to be used on binary option trading platforms. <br>
              Our products are used by experienced traders with limited time for analysis, newer traders who want to learn from our experts, <br>
              and traders of all levels looking to improve the consistency of their returns.</p>
        </div>

        <div class="hol-about-wrapper" id="ourproduct">
          <h2 class="hol-wwd-section-title" style="margin-top: 300px;">Our Product </h2>
          <p class="hol-about-paragraph">
            Wesignals are proud to offer 2 exchange's with a small monthly fee, that makes us attainable to all clients Worldwide. Our packages allow clients to benefit from our market knowledge, ongoing training, and professional customer liaison.
          </p>
        </div>

        <div class="w-row" style="margin-top: 50px;">
          <div class="rcorners2">
            Exchange 3<br><br><br>
            <p style="font-weight: 500; font-size: 36;">$99<p> <br>
            per month<br>
            <br>
            Unlimited live access to trend signals for<br>
             3 of our market indices<br>
            CAC40/AEX/BEL20<br>
            Up to 15 trades per week<br>
            15% Average monthly return<br>
            Minimum working capital suggested $2,500<br>
            <br>
            <a class="hol-nav-link w-nav-link" href="#top" style="max-width: 940px; font-family: Open Sans; color: #47b7e1; text-decoration: underline; font-weight:700;">PURCHASE</a>
            <br>
            <br>
          </div>
          <div class="rcorners2">
            Exchange 5<br><br><br>
            <p style="font-weight: 500; font-size: 36;">$149<p> <br>
            per month<br>
            <br>
            Unlimited live access to trend signals for<br>
             5 of our market indices<br>
            CAC40/AEX/BEL20/DAX/SMI<br>
            Up to 15 trades per week<br>
            25% Average monthly return<br>
            Minimum working capital suggested $2,500<br>
            <br>
            <a class="hol-nav-link w-nav-link" href="#top" style="max-width: 940px; font-family: Open Sans; color: #47b7e1; text-decoration: underline; font-weight:700;">PURCHASE</a>
            <br>
            <br>
          </div>
        </div>
      </div>
    </div>
  </div>
<!--- TESTIMONY --->
  <div class="hol-insta-section">
    <div class="w-container">

      <div class="hol-location-title-wrapper">
        <h2 class="hol-wwd-section-title testimony" style="font-family: MuseoSansW01-Rounded300;"><span style="font-weight: 300;">Client</span> Testimonials</h2>
        <?php
					echo "<img alt=\"wesignals: testimony_underline\" src=\"" . get_template_directory_uri() . "/assets/images/belowtestimony.jpg\" style=\"width:150px\"> ";
				?>
      </div>

      <div class="w-dyn-list">
        <div class="dyn-list w-clearfix w-dyn-items">
          <div class = "col-xs-12 col-md-3" style="text-align: right;">
              <?php
  							echo "<img alt=\"wesignals: left_kutip\" class=\"icon\" src=\"" . get_template_directory_uri() . "/assets/images/leftkutip.jpg\" style=\"height: 30; \"> ";
  						?>
          </div>
          <div class = "col-xs-12 col-md-6">
            <p style="font-family: MuseoSansW01-Rounded300;">Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>
            <div class = "col-xs-12 col-md-6">
              <p style="font-family: MuseoSansW01-Rounded300; color: #47b7e1; text-align: center;"> Terre Haute, IN</p>
            </div>
            <div class = "col-xs-12 col-md-6">
              <p style="font-family: MuseoSansW01-Rounded300; color: #252627; text-align: center;"> Marketing Manager</p>
            </div>
            <!--- draw the image -->
            <?php
							echo "<div class = \"col-xs-12 col-md-12\" style=\"text-align: center;\">
                <img alt=\"wesignals: abovename\" src=\"" . get_template_directory_uri() . "/assets/images/abovename.jpg\" style=\"width: 440;\">
                </div>";
              echo "<div class = \"col-xs-12 col-md-12\" style=\"text-align: center; z-index: 3;\">
                <img alt=\"wesignals: person\" src=\"" . get_template_directory_uri() . "/assets/images/testimony1.png\">
                </div>";
              echo "<div class = \"col-xs-12 col-md-12\" style=\"text-align: center; z-index: 1;\">
                <img alt=\"wesignals: ipad\" src=\"" . get_template_directory_uri() . "/assets/images/ipad.png\" style=\"margin-top: -50px;\">
                </div>";
            ?>
          </div>
          <div class = "col-xs-12 col-md-3">
              <?php
  							echo "<img alt=\"wesignals: right_kutip\" class=\"icon\" src=\"" . get_template_directory_uri() . "/assets/images/rightkutip.jpg\" style=\"height: 30;\"> ";
  						?>
          </div>

        </div>
      </div>

  </div>
</div>

<!-- CONTACT US -->
<div class="hol-contact-section" id="contactus">
  <div class="hol-val-wrapper rev" id="whychooseus">
    <div class="hol-val-div">
      <div class="hol-val-content-wrapper">
        <h2 class="hol-wwd-section-title" style="text-align: left;">Contact Us<br></h2>
        <p class="hol-about-paragraph" style="text-align: left; margin-top: 30px;margin-bottom: 40px;">
          Want to learn more about our trading capabilities or discuss your next trading?<br>
          Contact our team today.
        </p>
        <div class = "col-xs-12 col-md-12">
        <?php
          echo "<div class = \"col-xs-3 col-md-3\" style=\"text-align: left;\">
          <img alt=\"wesignals: location\" src=\"" . get_template_directory_uri() . "/assets/images/loc_icon.png\" style=\"height: 30;\">
          </div>";
        ?>
        <div class = "col-xs-9 col-md-9" style="text-align: left; color: white; position:relative; bottom: 0;">Location</div>
        </div>
        <?php
          echo "<img alt=\"wesignals: location\" class=\"icon\" src=\"" . get_template_directory_uri() . "/assets/images/phone_icon.png\" style=\"height: 30;\"> ";
        ?>
        <p style="text-align: left; margin-top: 30px;margin-bottom: 40px;">
          (+62) 813 7715 5483
        </p>
        <?php
          echo "<img alt=\"wesignals: location\" class=\"icon\" src=\"" . get_template_directory_uri() . "/assets/images/mail_icon.png\" style=\"height: 30;\"> ";
        ?>
        <p style="text-align: left; margin-top: 30px;margin-bottom: 40px;">
          wesignals@yahoo.com
        </p>
        <?php
          echo "<img alt=\"wesignals: location\" class=\"icon\" src=\"" . get_template_directory_uri() . "/assets/images/share_icon.png\" style=\"height: 30;\"> ";
        ?>
      </div>
    </div>
    <div class="hol-val-div">
      <!-- FORM CONTACT -->
      <?php
        echo "<img alt=\"wesignals: why choose us\" src=\"" . get_template_directory_uri() . "/assets/images/whychooseus.jpg\">";
       ?>
    </div>
  </div>
</div>

<!-- FOOTER -->
      <div class="hol-footer">
        <div class="w-container">
            <div class="footer-col-row w-row">
              <div class="w-col col-md-4">
                <div class="hol-footer-content-wrapper">
                  <div class="footer-title">GUESTS</div>
                    <ul class="w-list-unstyled">
                      <li class="li-item">
                        <a class="hol-footer-link" href="{% url= directory %}#about">How it works</a>
                      </li>
                    </ul>
                  </div>
                </div>
              <div class="w-col col-md-4">
                <div class="hol-footer-content-wrapper">
                <div class="footer-title">PARTNER</div>
                  <ul class="w-list-unstyled">
                    <li class="li-item">
                      <a class="hol-footer-link" href="{% url= directory %}#about">Become Partner</a>
                    </li>
                  </ul>
                </div>
              </div>
              <div class="w-col col-md-4"><div class="hol-footer-content-wrapper">
                <div class="footer-title">ABOUT</div>
                  <ul class="w-list-unstyled">
                    <li class="li-item">
                      <a class="hol-footer-link" href="{% url= directory %}#about">The Project</a>
                    </li>
                  </ul>
                </div>
              </div>
            </div>
          </div>

          <div>
            <div class="hol-val-wrapper rev">
              <div class="w-col w-col-6">
                <div>
                  <div class="cote footer-title"> ©wesignals, Inc. 2017</div>
                </div>
              </div>
              <div class="w-col w-col-6">
                <div>
                  <div class="w-row">
                    <div class="w-col w-col-3">
                    </div>
                    <div class="w-col w-col-3">
                      <strong>Follow Us On</strong> <br><br>
                    </div>
                    <div class="w-col w-col-3">
                      <div class="hol-social-icon-footer-wrapper">
                        <a class="icon-circle-div" href="http://facebook.com/wesignals">
                          <img src="asset/wesignals/fblogo.png" width="40">
                        </a>
                        <a class="icon-circle-div" href="http://instagram.com/wesignals.id">
                          <img src="asset/wesignals/Instagram.png" width="32">
                        </a>
                        <a class="icon-circle-div twi w-inline-block" href="#">
                          <img src="asset/wesignals/LINE_logo.png" width="32">
                        </a>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>

  <!--[if lte IE 9]>
    <script src="//cdnjs.cloudflare.com/ajax/libs/placeholders/3.0.2/placeholders.min.js"></script>
  <![endif]-->
  <?php
      echo "<script src =\"" . get_template_directory_uri() . "/assets/js/webflow.js\">  </script>";
   ?>
   <script>
   function adjustStyle(width) {
      width = parseInt(width);
      if (width < 701) {
        $("#pstyle").attr("style", "background-color: #dfdfe0;");
      }
    }

    $(function() {
      adjustStyle($(this).width());
      $(window).resize(function() {
        adjustStyle($(this).width());
      });
    });
    </script>
</body>
</html>
